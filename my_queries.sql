-- use database_name
use classicmodels

-- Display the items ordered according to date
select products.productName,orders.orderDate from ((products join orderdetails on products.productCode = orderdetails.productCode) join orders on orderdetails.orderNumber = orders.orderNumber) order by orders.orderDate;

-- Display each employee along with their customer 
select employees.employeeNumber,customers.customerName from employees left join customers on employees.employeeNumber=customers.salesRepEmployeeNumber;

--Display each customers along with their salesRep
select customers.customerName,employees.employeeNumber from employees right join customers on employees.employeeNumber=customers.salesRepEmployeeNumber;

--Find the office city of all the employees
select employees.employeeNumber,offices.city from employees inner join offices on employees.officeCode=offices.officeCode;

-- Find the total number of employees in each office.
select offices.city,sum(employees.employeeNumber) as total_employees from offices inner join employees on offices.officeCode=employees.officeCode group by offices.officeCode;

-- Total payment done by each customer
select customers.customerName,sum(payments.amount) as Amount from customers left join payments on customers.customerNumber = payments.customerNumber group by customers.customerName;

-- Display the employees in Boston
select employees.firstName,employees.lastName from employees join offices on employees.officeCode = offices.officeCode where city = 'Boston';


